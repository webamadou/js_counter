const counter = document.getElementById('counter');
const increment = document.getElementById('increment');
const decrement = document.getElementById('decrement');
let counting = 0;


/*const Incementor = () => {
  counting++;
  updateCounting();
  counter.style.color = counting === 0? 'initial':'green';
}
const Decrementor = () => {
  counting--;
  updateCounting();
  counter.style.color = counting === 0? 'initial':'red';
}
const updateCounting = () => {
    counter.innerHTML = counting;
}*/

const updatingCounter = (direction) => {
  let color = 'green';
  if(direction === 'down'){
    counting--;
    color = 'red';
  }
  else{
    counting++;
    color = 'green';
  }
  counter.innerHTML = counting;
  counter.style.color = counting === 0? 'initial': color;
}


increment.addEventListener('click', e => {
  e.preventDefault();
  updatingCounter('up');
})

decrement.addEventListener('click', e => {
  e.preventDefault();
  updatingCounter('down');
})
